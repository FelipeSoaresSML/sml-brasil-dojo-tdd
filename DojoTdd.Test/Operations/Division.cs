﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DojoTdd.Test
{
    [TestClass]
    public class Division
    {
        [TestMethod]
        public void DivideTwoNumbers()
        {
            var nums = new List<decimal>();
            nums.Add(123);
            nums.Add(342);

            var calc = new Calculator();

            var result = calc.Divide(nums.ToArray());

            Assert.AreEqual(nums.Aggregate((current, next) => current / next), result);
        }

        [TestMethod]
        public void DivideThreeNumbers()
        {
            var nums = new List<decimal>();
            nums.Add(123);
            nums.Add(342);
            nums.Add(342);

            var calc = new Calculator();

            var result = calc.Divide(nums.ToArray());

            Assert.AreEqual(nums.Aggregate((current, next) => current / next), result);
        }

        [TestMethod]
        public void DivideInfiniteNumbers()
        {
            var nums = new List<decimal>();

            var random = new Random().Next(0, 1000);
            for (int i = 0; i < random; i++)
                nums.Add(new Random().Next());

            var calc = new Calculator();
            var result = calc.Divide(nums.ToArray());

            Assert.AreEqual(nums.Aggregate((current, next) => current / next), result);
        }

        [TestMethod]
        public void DivideDecimalsNumbers()
        {
            var nums = new List<decimal>();
            nums.Add(123);
            nums.Add(342);
            nums.Add(342);
            nums.Add(342);
            nums.Add(4.5M);
            nums.Add(16.7M);

            var calc = new Calculator();

            var result = calc.Divide(nums.ToArray());

            Assert.AreEqual(nums.Aggregate((current, next) => current / next), result);
        }
    }
}
