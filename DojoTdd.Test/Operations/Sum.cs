﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace DojoTdd.Test
{
    [TestClass]
    public class Sum
    {
        [TestMethod]
        public void SumTwoNumbers()
        {
            var nums = new List<decimal>();
            nums.Add(123);
            nums.Add(342);

            var calc = new Calculator();

            var result = calc.Sum(nums.ToArray());

            Assert.AreEqual(nums.Aggregate((current, next) => current + next), result);
        }

        [TestMethod]
        public void SumThreeNumbers()
        {
            var nums = new List<decimal>();
            nums.Add(123);
            nums.Add(342);
            nums.Add(342);

            var calc = new Calculator();

            var result = calc.Sum(nums.ToArray());

            Assert.AreEqual(nums.Aggregate((current, next) => current + next), result);
        }

        [TestMethod]
        public void SumInfiniteNumbers()
        {
            var nums = new List<decimal>();

            var random = new Random().Next(0, 1000);
            for (int i = 0; i < random; i++)
                nums.Add(new Random().Next());

            var calc = new Calculator();
            var result = calc.Sum(nums.ToArray());

            Assert.AreEqual(nums.Aggregate((current, next) => current + next), result);
        }

        [TestMethod]
        public void SumDecimalsNumbers()
        {
            var nums = new List<decimal>();
            nums.Add(123);
            nums.Add(342);
            nums.Add(342);
            nums.Add(342);
            nums.Add(4.5M);
            nums.Add(16.7M);

            var calc = new Calculator();

            var result = calc.Sum(nums.ToArray());

            Assert.AreEqual(nums.Aggregate((current, next) => current + next), result);
        }
    }
}
