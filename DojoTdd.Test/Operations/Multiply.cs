﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DojoTdd.Test
{
    [TestClass]
    public class Multiply
    {
        [TestMethod]
        public void MultiplyTwoNumbers()
        {
            var nums = new List<decimal>();
            nums.Add(123);
            nums.Add(342);

            var calc = new Calculator();

            var result = calc.Multiply(nums.ToArray());

            Assert.AreEqual(nums.Aggregate((current, next) => current * next), result);
        }

        [TestMethod]
        public void MultiplyThreeNumbers()
        {
            var nums = new List<decimal>();
            nums.Add(123);
            nums.Add(342);
            nums.Add(342);

            var calc = new Calculator();

            var result = calc.Multiply(nums.ToArray());

            Assert.AreEqual(nums.Aggregate((current, next) => current * next), result);
        }

        [TestMethod]
        public void MultiplyInfiniteNumbers()
        {
            var nums = new List<decimal>();

            var random = new Random().Next(0, 15);
            for (int i = 0; i < random; i++)
                nums.Add(new Random().Next(0, 100));

            var calc = new Calculator();
            var result = calc.Multiply(nums.ToArray());

            Assert.AreEqual(nums.Aggregate((current, next) => current * next), result);
        }

        [TestMethod]
        public void MultiplyDecimalsNumbers()
        {
            var nums = new List<decimal>();
            nums.Add(123);
            nums.Add(342);
            nums.Add(342);
            nums.Add(342);
            nums.Add(4.5M);
            nums.Add(16.7M);

            var calc = new Calculator();

            var result = calc.Multiply(nums.ToArray());

            Assert.AreEqual(nums.Aggregate((current, next) => current * next), result);
        }
    }
}
