﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DojoTdd
{
    public class Calculator
    {
        public decimal Sum(params decimal[] nums)
        {
            return calculate(nums.ToList(), (current, next) => current + next);
        }

        public decimal Minus(params decimal[] nums)
        {
            return calculate(nums.ToList(), (current, next) => current - next);
        }

        public decimal Divide(params decimal[] nums)
        {
            return calculate(nums.ToList(), (current, next) => current / next);
        }

        public decimal Multiply(params decimal[] nums)
        {
            return calculate(nums.ToList(), (current, next) => current * next);
        }

        private decimal calculate(List<decimal> nums, Func<decimal, decimal, decimal> operation)
        {
            return nums.Aggregate(operation);
        }
    }
}
